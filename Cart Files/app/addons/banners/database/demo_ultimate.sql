UPDATE ?:banners SET company_id = 1;
UPDATE ?:banners SET company_id = 2 WHERE banner_id = 4;

REPLACE INTO `?:ult_objects_sharing` (`share_company_id`, `share_object_id`, `share_object_type`) VALUES (1, '2', 'banners');
REPLACE INTO `?:ult_objects_sharing` (`share_company_id`, `share_object_id`, `share_object_type`) VALUES (2, '2', 'banners');
REPLACE INTO `?:ult_objects_sharing` (`share_company_id`, `share_object_id`, `share_object_type`) VALUES (1, '3', 'banners');
REPLACE INTO `?:ult_objects_sharing` (`share_company_id`, `share_object_id`, `share_object_type`) VALUES (2, '4', 'banners');
REPLACE INTO `?:ult_objects_sharing` (`share_company_id`, `share_object_id`, `share_object_type`) VALUES (1, '5', 'banners');
